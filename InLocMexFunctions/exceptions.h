#pragma once
#include <stdexcept>
#include <string>

/// <summary>
/// Thown after invalid indexing
/// </summary>
class indexing_exception : public std::exception {
public:
	indexing_exception(const std::string& what) : std::exception(what.c_str())
	{}

	indexing_exception() : std::exception("Invalid index")
	{}
};
