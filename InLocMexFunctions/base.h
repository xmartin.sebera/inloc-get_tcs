#pragma once

//// Add MKL libraries in Visual Studio
//#pragma comment(lib, "mkl_core.lib")
//#pragma comment(lib, "mkl_intel_lp64.lib")
////#pragma comment(lib, "mkl_lapack95_lp64.lib")
//#pragma comment(lib, "mkl_scalapack_lp64.lib")
//#pragma comment(lib, "mkl_sequential.lib")

// Visual annotation of output reference parameters
#define OUT

// Most important includes
#include <mkl.h>
#include <vector>
#include <map>
#include <string>
#include <iostream>
#define EIGEN_USE_MKL_ALL
#include "Eigen"
#include "StdVector"
#include <fstream>
#include <chrono>
#include <omp.h>

using Eigen::Index;
using Eigen::VectorXd;
using Eigen::VectorXf;
using Eigen::MatrixXd;
using Eigen::MatrixXf;
using Eigen::Dynamic;
using Eigen::Matrix;

// Eigen matrices vectors need this specific allocator.
// Using vector<MatrixXf> leads to memory errors. Use matrix_vector instead!
typedef std::vector<MatrixXf, Eigen::aligned_allocator<MatrixXf>> matrix_vector;

typedef std::vector<Eigen::Map<MatrixXf>, Eigen::aligned_allocator<Eigen::Map<MatrixXf>>> matrix_map_vector;

// Tools for code profiling. Disable profiling before releasing final version!!
// Prints a message to standard output (if ENABLE_DEBUG_COUT is defined)
#ifdef ENABLE_DEBUG_COUT
#define DCOUT(message) {std::cout << message << std::endl;}
#else
#define DCOUT(message)
#endif

// _TIC creates a timer
// _TOC prints a message and elapsed time to standard output
// _RETIC 
#ifdef ENABLE_PROFILING
#define _TIC auto clckstart = chrono::system_clock::now();
#define _RETIC clckstart = chrono::system_clock::now();
#define _TOC(message) {auto clckend = chrono::system_clock::now();std::cout << message << ": " << chrono::duration_cast<chrono::milliseconds>(clckend - clckstart).count() << " ms" << endl;}
#else
#define _TIC
#define _RETIC
#define _TOC(message)
#endif