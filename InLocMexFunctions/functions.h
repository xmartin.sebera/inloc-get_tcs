#pragma once
#include "base.h"

/// <summary>
/// Finds tentative correspondencies between image pair (query image <-> database image)
/// </summary>
/// <param name="descriptorsQ">descriptors of features in query image</param>
/// <param name="descriptorsDBs">descriptors of features in every db image</param>
/// <returns>Tentative correspondencies (based on mutually nearest neighbor)</returns>
std::vector<std::map<size_t, size_t>> get_tcs(
	const Eigen::Map<MatrixXf>& descriptorsA,
	const std::vector<Eigen::Map<MatrixXf>>& descriptorsB);