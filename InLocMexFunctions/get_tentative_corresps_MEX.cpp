//#define ENABLE_DEBUG_COUT
//#define ENABLE_PROFILING
#include <string>
#include <list>
#include "functions.h"

#include <mex.h>

using namespace std;

/// <summary>
/// Main function of MEX library.
/// Finds tentative correspondencies
/// </summary>
/// <param name="nlhs">number of output parameters</param>
/// <param name="plhs">output parameters</param>
/// <param name="nrhs">number of input parameters</param>
/// <param name="prhs">input parameters</param>
void mexFunction(int nlhs, mxArray* plhs[],
    int nrhs, const mxArray* prhs[])
{
    if (nrhs != 2)
        mexErrMsgTxt("Invalid number of input arguments - 2 required.");

    if (nlhs != 1)
        mexErrMsgTxt("1 output argument required");
    
    const int des1_r = mxGetM(prhs[0]);
    const int des1_c = mxGetN(prhs[0]);
    float* desc1 = (float*)mxGetPr(prhs[0]);
    Eigen::Map<MatrixXf> desc1_mat(desc1, des1_r, des1_c);

    const auto dims = mxGetDimensions(prhs[1]);
    const mwSize num_desc2 = std::max(dims[0], dims[1]);
    
    // Descriptors of db images (cell with 2d arrays)
    const mxArray* descs2_cell = prhs[1];
    vector<float*> descs2_ptrs(num_desc2); // pointers to descriptors data
    vector<Eigen::Map<MatrixXf>> descs2; // descriptors data mapped to matrices

    mxArray* cellElement;
    for (mwIndex i = 0; i < num_desc2; ++i) {
        cellElement = mxGetCell(descs2_cell, i);
        descs2_ptrs[i] = (float*)mxGetPr(cellElement);
        int rws = mxGetM(cellElement);
        int cls = mxGetN(cellElement);
        descs2.push_back(Eigen::Map<MatrixXf>(descs2_ptrs[i], rws, cls));
    }

    // Tentative correspondencies for each image pair (query <-> db)
    vector<map<size_t, size_t>> matches = get_tcs(desc1_mat, descs2);

    // Send correspondencies into Matlab's memory
    auto cell_array_ptr = mxCreateCellMatrix(num_desc2, 1);
    for (mwIndex i = 0; i < num_desc2; ++i) {
        auto arr_corresps = mxCreateNumericMatrix(2, matches[i].size(), mxINT32_CLASS, mxREAL);
        int* assign = (int*)mxGetPr(arr_corresps);
        size_t j = 0;
        for (const auto& match : matches[i]) {
            assign[j] = match.first;
            assign[j + 1] = match.second;
            j += 2;
        }
        mxSetCell(cell_array_ptr, i, arr_corresps);
    }
    plhs[0] = cell_array_ptr;
}